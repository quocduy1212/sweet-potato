# Async action handler

# Usage
1. Library to simplify async promise base action handler
2. Only define one action for an async action
3. The async handler will create `_loading`, `_success`, `_error` actions and update store when the promise is resolve or reject
4. Provide selector for async action to query the action status: `isLoading`, `isSuccess`, `isError`, `response`

# Setup

## Async action type
* Structure:
  + `['ACTION_TYPE', getter, setter]`
  + `getter` and `setter` are for reducer to get and set action data to redux state
* Example
    [
      TYPES.MY_ASYNC_ACTION,
      state => location,
      (state, newState) => ({
        location: {
          ...newState,
        },
      }),
    ]
  + Above action mean: define an async named `MY_ASYNC_ACTION` and save it to `state.location`

## Create reducers for async action
    import asyncTypes from '../constants/async-actions';
    import { loadAsyncTypes } from 'async';

    const initState = { location: {} };
    const asyncReducers = loadAsyncTypes(asyncTypes, initState);

## Add asyncMiddleware and combine async reducers to redux when creating store
    import { createStore, applyMiddleware, combineReducers } from 'redux';
    import { asyncMiddleware } from 'async';
    const rootReducer = combineReducers({ asyncActions: asyncReducers, });
    const store = createStore(rootReducer, {}, applyMiddleware(asyncMiddleware));

## Async selector
    import { asyncSelector } from '../async';
    const selector = asyncSelector(state.asyncAction);
    /*
     * selector = {
     *   isLoading,
     *   isError,
     *   isSuccess,
     *   response,
     *   error,
     * }
    */

# Development
1. `yarn install`
2. `yarn dev` to start dev server at `http://localhost:3333`
3. `yarn test` to run unit test
