import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import rootReducer from '../reducers';
import { asyncMiddleware } from '../async';

const enhancer = composeWithDevTools(applyMiddleware(asyncMiddleware));
const store = createStore(rootReducer, {}, enhancer);

export default store;
