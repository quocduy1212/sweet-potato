import { combineReducers } from 'redux';
import asyncTypes from '../constants/async';
import app from './app';
import { loadAsyncTypes } from '../async';

const asyncReducers = loadAsyncTypes(asyncTypes, {
  location: {},
});

const rootReducer = combineReducers({
  app,
  asyncActions: asyncReducers,
});

export default rootReducer;
