import * as TYPES from '../constants/types';

export default [
  [
    TYPES.MY_ASYNC_ACTION,
    state => location,
    (state, data) => ({
      location: {
        ...location,
        ...data,
      },
    }),
  ],
];
