class AsyncManager {
  ASYNC_TYPE_MANAGER = {};

  loadAsyncTypes = (TYPES = [], DEFAULT_STATE) => {
    const reducers = {};
    TYPES.forEach(([TYPE, getter, setter]) => {
      const loading = `${TYPE}_LOADING`;
      const success = `${TYPE}_SUCCESS`;
      const error = `${TYPE}_ERROR`;

      this.ASYNC_TYPE_MANAGER[TYPE] = {
        loading,
        success,
        error,
        getter,
        setter,
      };
      reducers[loading] = (state, action) =>
        setter(state, {
          ...getter(state),
          type: loading,
        });
      reducers[success] = (state, action) =>
        setter(state, {
          ...getter(state),
          type: success,
          response: action.response,
        });
      reducers[error] = (state, action) =>
        setter(state, {
          ...getter(state),
          type: error,
          error: action.error,
        });
    });
    const asyncReducers = (state = DEFAULT_STATE, action) =>
      reducers[action.type] ? reducers[action.type](state, action) : state;
    return asyncReducers;
  };

  getAsyncType = type => this.ASYNC_TYPE_MANAGER[type];
}

export default new AsyncManager();
