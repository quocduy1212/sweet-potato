export default asyncState => {
  const type = asyncState.type || '';
  return {
    isLoading: type.endsWith('_LOADING'),
    isSuccess: type.endsWith('_SUCCESS'),
    isError: type.endsWith('_ERROR'),
    response: asyncState.response,
    error: asyncState.error,
  };
};
