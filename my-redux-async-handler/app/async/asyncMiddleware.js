import AsyncManager from './asyncManager';

const asyncMiddleware = store => next => action => {
  if (action.payload && action.payload.promise && action.payload.promise.then) {
    const promise = action.payload.promise;
    const asyncActionType = AsyncManager.getAsyncType(action.type);
    if (asyncActionType) {
      const loadingAction = {
        type: asyncActionType.loading,
      };
      promise.then(
        response => {
          const successAction = {
            type: asyncActionType.success,
            response: response,
          };
          next(successAction);
        },
        error => {
          const errorAction = {
            type: asyncActionType.error,
            error,
          };
          next(errorAction);
        }
      );
      next(loadingAction);
    }
  }
  return next(action);
};

export default asyncMiddleware;
