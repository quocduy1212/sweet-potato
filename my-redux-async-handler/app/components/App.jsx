import React from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { asyncSelector } from '../async';

import styles from './App.scss';

const App = ({ isLoading, isSuccess, response, fetchTheWorld }) => (
  <div>
    <div className={styles.bold}>hola</div>
    {isLoading && <div>loading...</div>}
    {isSuccess && <div>{response}</div>}
    <button onClick={fetchTheWorld}>fetch</button>
  </div>
);

const mapStateToProps = ({ asyncActions }) => ({
  ...asyncSelector(asyncActions.location),
});

export default connect(mapStateToProps, {
  fetchTheWorld: Actions.fetchTheWorld,
})(App);
