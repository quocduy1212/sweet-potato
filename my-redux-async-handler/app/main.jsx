/* eslint-disable global-require */
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';

import 'app-styles/main';

import store from './store/configure-store';
import Root from './root';

render(<Root store={store} />, document.getElementById('phamduy-container'));
