import * as TYPES from '../constants/types';

const fakePromise = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => resolve('hola'), 1000);
  });

export const fetchTheWorld = () => ({
  type: TYPES.MY_ASYNC_ACTION,
  payload: {
    promise: fakePromise(),
  },
});
