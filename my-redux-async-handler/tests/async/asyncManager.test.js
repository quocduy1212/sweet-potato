import AsyncManager from '../../app/async/asyncManager';

test('loadAsyncTypes', () => {
  const TYPE = 'MY_LOADING';
  const STATE = { location: {} };
  const manager = new AsyncManager();
  const reducer = manager.loadAsyncTypes([
    [
      TYPE,
      state => state.location,
      (state, data) => ({
        ...state,
        location: {
          ...state.location,
          ...data,
        },
      }),
    ],
  ]);

  const actionType = manager.ASYNC_TYPE_MANAGER[TYPE];
  expect(actionType).toBeDefined();

  // test loading reducer
  const loadingState = reducer(STATE, { type: actionType.loading });
  expect(loadingState.location.type).toEqual(actionType.loading);

  // test success reducer
  const RESPONSE = 'fake response';
  const successState = reducer(STATE, {
    type: actionType.success,
    response: RESPONSE,
  });
  expect(successState.location.type).toEqual(actionType.success);
  expect(successState.location.response).toEqual(RESPONSE);

  // test error reducer
  const ERROR = 'some error';
  const errorState = reducer(STATE, {
    type: actionType.error,
    error: ERROR,
  });
  expect(errorState.location.type).toEqual(actionType.error);
  expect(errorState.location.error).toEqual(ERROR);
});
