const TAGNAME = {
  TEXT_NODE: 'TEXT',
  UNKNOW: 'UNKNOW',
};

const getNodeTagName = node => {
  if (!node) {
    return TAGNAME.UNKNOW;
  } else if (typeof node === 'string' || node instanceof Text) {
    return TAGNAME.TEXT_NODE;
  }
  return node.tagName;
};

const isTextNode = node => getNodeTagName(node) === TAGNAME.TEXT_NODE;

const createDomElmFromVDom = vdom => {
  if (isTextNode(vdom)) {
    return document.createTextNode(vdom);
  }
  const domElm = document.createElement(vdom.tagName);
  if (vdom.class) {
    domElm.setAttribute('class', vdom.class);
  }
  (vdom.children || []).forEach(c => {
    domElm.appendChild(createDomElmFromVDom(c));
  });
  return domElm;
};

const updateInnerText = (vdom, domElm) => {
  if (isTextNode(vdom) && isTextNode(domElm)) {
    const vdomText = vdom;
    const domElmText = domElm.textContent;
    if (vdomText !== domElmText) {
      domElm.parentNode.replaceChild(createDomElmFromVDom(vdom), domElm);
    }
  }
};

const updateClass = (vdom, domElm) => {
  if (vdom.class !== domElm.className) {
    domElm.setAttribute('class', vdom.class);
  }
};

const updateAttrs = (vdom, domElm) => {
  updateInnerText(vdom, domElm);
  updateClass(vdom, domElm);
  // #TODO update attributes
};

const removeExtraElmInDom = (vdom, domElm) => {
  if (vdom.children.length < domElm.childNodes.length) {
    for (let i = vdom.children.length; i < domElm.childNodes.length; i++) {
      domElm.childNodes[i].remove();
    }
  }
};

const updateDom = (vdom, domElm, parentNode) => {
  let domElmType = getNodeTagName(domElm);
  let vdomType = getNodeTagName(vdom);
  if (vdomType.toUpperCase() !== domElmType.toUpperCase()) {
    if (domElm) {
      parentNode.replaceChild(createDomElmFromVDom(vdom), domElm);
    } else {
      parentNode.appendChild(createDomElmFromVDom(vdom));
    }
  } else {
    // updating attributes diff
    updateAttrs(vdom, domElm);
    if (vdom.children && vdom.children.length) {
      removeExtraElmInDom(vdom, domElm);
      vdom.children.forEach((vd, index) =>
        updateDom(
          vd,
          domElm.childNodes.length > index
            ? domElm.childNodes[index]
            : undefined,
          domElm
        )
      );
    }
  }
};

export default () => {
  const mainContainer = document.getElementById('phamduy-container');
  const vdom1 = {
    tagName: 'div',
    children: [
      'hello',
      { tagName: 'strong', class: 'm-h_10', children: ['pham'] },
      'hello',
    ],
  };

  const vdom2 = {
    tagName: 'div',
    children: [
      'hello',
      { tagName: 'strong', class: 'm-l_10', children: ['pham'] },
      { tagName: 'strong', class: 'm-l_10', children: ['___'] },
      { tagName: 'i', children: ['hello'] },
      { tagName: 'i', children: ['italic'] },
    ],
  };

  const elm = createDomElmFromVDom(vdom1);
  mainContainer.appendChild(elm);
  let counter = 1;

  const buttonElm = document.createElement('button');
  buttonElm.innerText = 'do update';
  buttonElm.addEventListener('click', () => {
    if (counter % 2) {
      updateDom(vdom2, elm, mainContainer);
    } else {
      updateDom(vdom1, elm, mainContainer);
    }
    counter++;
  });
  document.body.appendChild(buttonElm);
};
