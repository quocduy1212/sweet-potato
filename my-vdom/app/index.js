import 'babel-polyfill';
import vdom from './js/vdom';
import './main.scss';

document.addEventListener('DOMContentLoaded', function(event) {
  vdom();
});
