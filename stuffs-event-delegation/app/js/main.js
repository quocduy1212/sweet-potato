import { fetchUserList } from '../mock';

const createElement = (type, innerHtml, attrs = {}) => {
  const result = document.createElement(type);
  if (innerHtml) {
    result.innerHTML = innerHtml;
  }
  Object.keys(attrs).forEach(a => {
    result.setAttribute(a, attrs[a]);
  });
  return result;
};

export default () => {
  console.log('main');

  const containerDOM = document.getElementById('phamduy-container');
  containerDOM.addEventListener('click', event => {
    const dataset = event.target.dataset;
    if (dataset.deleteButton === 'yes') {
      const targetUser = containerDOM.querySelector(
        `div[data-user-id="${dataset.userId}"]`
      );
      if (targetUser) {
        targetUser.remove();
      }
    }
  });
  fetchUserList().then(users => {
    users.forEach(user => {
      const userElm = createElement('div', undefined, {
        class: 'user',
        'data-user-id': user.id,
      });
      const deleteElm = createElement('i', 'close', {
        class: 'material-icons delete-user',
        'data-user-id': user.id,
        'data-delete-button': 'yes',
      });
      const userNameElm = createElement('div', user.name, {
        class: 'user-name',
      });
      const userAvatarContainerElm = createElement('div', undefined, {
        class: 'user-avatar',
      });
      const userAvatarElm = createElement('img', undefined, {
        src: user.avatarUrl,
      });
      userAvatarContainerElm.appendChild(userAvatarElm);
      userElm.appendChild(userAvatarContainerElm);
      userElm.appendChild(userNameElm);
      userElm.appendChild(deleteElm);
      containerDOM.appendChild(userElm);
    });
  });
};
