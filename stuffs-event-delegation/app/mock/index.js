import faker from 'faker';
const MAX_USER_NO = 50;

export const fetchUserList = () => {
  const results = [];
  for (var i = 0; i < MAX_USER_NO; i++) {
    results.push({
      id: faker.random.uuid(),
      name: faker.name.findName(),
      avatarUrl: faker.image.avatar(),
    });
  }
  return new Promise(resolve => {
    setTimeout(() => resolve(results), 1000);
  });
};
