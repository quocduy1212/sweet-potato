# Small fun playground with
1. Flex
2. Animation
3. Event delegation

# Development
1. `yarn install`
2. `yarn dev` to start dev server at `http://localhost:3333`
3. `yarn test` to run unit test
