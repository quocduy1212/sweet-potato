# Simple implementation of Promise

# Available functions
1. `MyPromise.resolve`
2. `MyPromise.reject`
3. `new MyPromise((resolve, reject) => {}).then(...).then(...).catch(...)`
4. `MyPromise.all([...])`
5. `MyPromise.race([...])`

# Development
1. `yarn install`
2. `yarn dev` to start dev server at `http://localhost:3333`
3. `yarn test` to run unit test
