import 'babel-polyfill';
import main from './js/main';
import './main.scss';

document.addEventListener('DOMContentLoaded', function(event) {
  main();
});
