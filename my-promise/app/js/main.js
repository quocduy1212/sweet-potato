import MyPromise from './my-promise';

const test1 = () => {
  // #1 Constructor
  new MyPromise(res => {
    console.log('#1 Constructor');
    setTimeout(() => res('world'), 1000);
  }).then(console.log);
};

const test2 = () => {
  // #2 chained then
  new MyPromise(res => {
    console.log('#2 chained then');
    setTimeout(() => res('world'), 1000);
  })
    .then(msg => msg)
    .then(console.log);
};

const test3 = () => {
  // #3 then with catch
  new MyPromise(res => {
    console.log('#3 then with catch');
    setTimeout(() => res('world'), 1000);
  })
    .then(() => {
      throw new Error('caught world');
    })
    .catch(e => {
      console.error(e.message);
    });
};

const test4 = () => {
  // #4 then skip with catch
  new MyPromise(res => {
    console.log('#4 then skip with catch');
    setTimeout(() => res('world'), 1000);
  })
    .then(() => {
      throw new Error('world');
    })
    .then(() => {
      console.log('never');
    })
    .catch(e => {
      console.error(e.message);
    });
};

const test5 = () => {
  // #5 then catch resuming
  new MyPromise(res => {
    console.log('#5 then catch resuming');
    setTimeout(() => res('world'), 1000);
  })
    .then(() => {
      throw new Error('world');
    })
    .then(() => {
      console.log('never');
    })
    .catch(e => {
      console.error(e.message);
      return 'caught';
    })
    .then(console.log);

  new MyPromise(res => {
    console.log('#5 then catch resuming');
    setTimeout(() => res('world'), 1000);
  })
    .then(() => {
      throw new Error('world 1');
    })
    .then(() => {
      console.log('never 1');
    })
    .catch(e => {
      console.error(e.message);
      return 'caught 1';
    })
    .then(() => {
      throw new Error('world 2');
    })
    .then(() => {
      console.log('never 2');
    })
    .catch(e => {
      console.error(e.message);
      return 'caught 2';
    })
    .then(console.log);
};

const test6 = () => {
  // #6 all
  new MyPromise(res => {
    console.log('$6 all');
    setTimeout(() => res('world'), 1000);
  }).then(() => {
    const simpleAll = MyPromise.all([
      new MyPromise(res => res('hello')),
      new MyPromise(res => res('world')),
    ]);
    simpleAll.then(console.log);
  });

  new MyPromise(res => {
    console.log('$6 all');
    setTimeout(() => res('world'), 1000);
  }).then(() => {
    const p1 = new MyPromise(res => {
      res('hello');
    });
    const p2 = new MyPromise(res => {
      res('world');
    }).then(() => {
      throw new Error('boom');
    });
    MyPromise.all([p1, p2])
      .then(console.log)
      .catch(console.error);
  });
};

const test7 = () => {
  // #7 race
  MyPromise.race([
    new MyPromise(res => setTimeout(() => res(500), 500)),
    new MyPromise(res => setTimeout(() => res(100), 100)),
  ]).then(console.log);
};

const test8 = () => {
  // #8 async work
  const p2 = new MyPromise(res => res())
    .then(() => {
      return new MyPromise(res => {
        setTimeout(() => {
          console.log('hello');
          res();
        }, 1000);
      });
    })
    .then(() => {
      console.log('world');
    });
};
export default () => {
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  test8();
};
