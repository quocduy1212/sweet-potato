const PENDING = 0;
const FULLFILLED = 1;
const REJECTED = 2;
const noop = () => {};

export default class MyPromise {
  state = PENDING;
  result = undefined;
  cbs = [];
  currentCbIdx = 0;

  constructor(fn) {
    fn(this.fullfill, this.reject);
  }

  pending = () => {
    this.state = PENDING;
  };

  fullfill = success => {
    const prevState = this.state;
    this.state = FULLFILLED;
    this.result = success;
    if (prevState === PENDING) {
      // promise resume from pending, trigger all on-hold (not called) cbs
      this.triggerCbs();
    }
  };

  reject = err => {
    const prevState = this.state;
    this.state = REJECTED;
    this.result = err;
    if (prevState === PENDING) {
      // promise resume from pending, trigger all on-hold (not called) cbs
      this.triggerCbs();
    }
  };

  triggerCbs = () => {
    for (
      ;
      this.currentCbIdx < this.cbs.length && this.state !== PENDING;
      this.currentCbIdx++
    ) {
      this.executeCb(
        this.cbs[this.currentCbIdx].onFullfill,
        this.cbs[this.currentCbIdx].onReject
      );
    }
  };

  executeCb = (onFullfill, onReject) => {
    try {
      let result;
      if (this.state === FULLFILLED) {
        result = onFullfill(this.result);
      }
      if (this.state === REJECTED) {
        result = onReject(this.result);
      }
      if (result) {
        if (typeof result.then === 'function') {
          // wait for child promise to finish first
          this.pending();
          result.then(
            data => {
              this.fullfill(data);
            },
            err => {
              this.reject(err);
            }
          );
        } else {
          this.fullfill(result);
        }
      }
    } catch (ex) {
      this.reject(ex);
    }
  };

  saveCb = (onFullfill, onReject) => {
    if (this.state === PENDING) {
      this.cbs.push({
        onFullfill,
        onReject,
      });
    } else {
      this.executeCb(onFullfill, onReject);
    }
  };

  then = (onFullfill = noop, onReject = noop) => {
    this.saveCb(onFullfill, onReject);
    return this;
  };

  catch = onReject => {
    this.saveCb(() => {}, onReject);
    return this;
  };

  static resolve = result => {
    return new MyPromise((resolve, reject) => {
      if (typeof result.then === 'function') {
        result.then(
          data => {
            resolve(data);
          },
          err => {
            reject(err);
          }
        );
      } else {
        resolve(result);
      }
    });
  };

  static reject = err =>
    new MyPromise((resolve, reject) => {
      reject(err);
    });

  static all = (promises = []) => {
    return new MyPromise((resolve, reject) => {
      const results = new Array(promises.length);
      let resolved = 0;

      promises.forEach((prs, index) => {
        prs.then(
          data => {
            results[index] = data;
            resolved++;
            if (resolved === promises.length) {
              resolve(results);
            }
          },
          err => {
            reject(err);
          }
        );
      });
    });
  };

  static race = (promises = []) => {
    return new MyPromise((resolve, reject) => {
      promises.forEach(prs => {
        prs.then(data => {
          resolve(data);
        });
      });
    });
  };
}
