const HALF_SQUARE_SIZE = 20;
const DISTANCE = 100;
const DEFAULT_X = '50%';
const DEFAULT_Y = '50%';

const randomInt = () => Math.floor(Math.random() * Math.floor(255));

const getRandomColor = () => {
  return `rgb(${randomInt()}, ${randomInt()}, ${randomInt()})`;
};

const shouldTransform = (x, y, mainContainer) =>
  x < DISTANCE ||
  y < DISTANCE ||
  x + DISTANCE > mainContainer.clientWidth ||
  y + DISTANCE > mainContainer.clientHeight;

const createSquare = (x = DEFAULT_X, y = DEFAULT_Y) => {
  let mouseDownOnSquare = false;
  let currentColor = 'red';
  const mainContainer = document.getElementById('phamduy-container');
  const squareElm = document.createElement('div');
  squareElm.addEventListener('mousedown', event => {
    mouseDownOnSquare = true;
  });
  squareElm.addEventListener('mouseup', event => {
    mouseDownOnSquare = false;
  });
  squareElm.setAttribute('class', 'square');
  squareElm.setAttribute('style', `top: ${y}; left: ${x};`);
  mainContainer.appendChild(squareElm);
  mainContainer.addEventListener('mousemove', event => {
    let x = event.clientX - HALF_SQUARE_SIZE;
    let y = event.clientY - HALF_SQUARE_SIZE;

    if (x < HALF_SQUARE_SIZE) {
      x = 0;
    } else if (x + HALF_SQUARE_SIZE * 2 > mainContainer.clientWidth) {
      x = mainContainer.clientWidth - HALF_SQUARE_SIZE * 2;
    }

    if (y < HALF_SQUARE_SIZE) {
      y = 0;
    } else if (y + HALF_SQUARE_SIZE * 2 > mainContainer.clientHeight) {
      y = mainContainer.clientHeight - HALF_SQUARE_SIZE * 2;
    }

    if (mouseDownOnSquare) {
      let style = '';
      if (shouldTransform(x, y, mainContainer)) {
        if (currentColor === 'red') {
          currentColor = getRandomColor();
        }
        style += 'border-radius: 50%;';
        style += 'transition: border-radius 1s;';
        style += `background-color: ${currentColor};`;
      } else {
        style += 'border-radius: unset;';
        style += 'transition: unset;';
      }
      if (!shouldTransform(x, y, mainContainer)) {
        currentColor = 'red';
        style += `background-color: ${currentColor};`;
      }
      style += `top: ${y}px; left: ${x}px;`;
      squareElm.setAttribute('style', style);
    }
  });
  return squareElm;
};

export default () => {
  const mainContainer = document.getElementById('phamduy-container');

  createSquare();
  createSquare('10px', '10px');
};
