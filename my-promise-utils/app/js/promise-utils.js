export const first = (promises = []) => {
  let rejectedPromise = 0;
  return new Promise((resolve, reject) => {
    promises.forEach((p, index) =>
      Promise.resolve(p)
        .then(result => resolve(result))
        .catch(() => {
          rejectedPromise++;
          if (rejectedPromise === promises.length) {
            reject('Oops');
          }
        })
    );
  });
};

export const last = (promises = []) => {
  let counter = 0;
  return new Promise((resolve, reject) => {
    promises.forEach((p, index) =>
      Promise.resolve(p)
        .then(result => {
          counter++;
          if (counter === promises.length) {
            resolve(result);
          }
        })
        .catch(() => {
          counter++;
          if (counter === promises.length) {
            reject('Oops');
          }
        })
    );
  });
};

export const none = (promises = []) => {
  let counter = 0;
  return new Promise((resolve, reject) => {
    promises.forEach((p, index) =>
      Promise.resolve(p)
        .then(() => {
          reject('Oops');
        })
        .catch(() => {
          counter++;
          if (counter === promises.length) {
            resolve('None');
          }
        })
    );
  });
};

export const map = (promises = [], cb) =>
  Promise.all(
    promises.map(
      p =>
        new Promise(resolve =>
          Promise.resolve(p).then(value => cb(value, resolve))
        )
    )
  );

export const PromiseCollection = (promises = []) => ({
  map: cb => {
    const newPromises = promises.map(
      p =>
        new Promise(resolve => {
          Promise.resolve(p).then(value => {
            cb(value, resolve);
          });
        })
    );
    return PromiseCollection(newPromises);
  },
  val: cb => Promise.all(promises).then(results => cb(results)),
});

class MyPromiseCollection {
  constructor(promises) {
    this.promises = promises;
  }

  map = cb => {
    this.promises = this.promises.map(
      p =>
        new Promise(resolve => {
          Promise.resolve(p).then(value => {
            cb(value, resolve);
          });
        })
    );
    return this;
  };

  val = cb => Promise.all(this.promises).then(results => cb(results));
}

export const createCollection = (promises = []) =>
  new MyPromiseCollection(promises);

export const ajax = (url, cb) => setTimeout(() => cb('success'));

export const cbWrapper = ajaxFn => (...args) =>
  new Promise((resolve, reject) => {
    args.push((success, err) => {
      if (success) {
        resolve(success);
      } else {
        reject(err);
      }
    });
    ajaxFn(...args);
  });

export const cbWrapperNonEs6 = ajaxFn => () => {
  const args = Array.prototype.slice.call(arguments);
  return new Promise((resolve, reject) => {
    args.push((success, err) => {
      if (success) {
        resolve(success);
      } else {
        reject(err);
      }
    });
    ajaxFn.apply(null, args);
  });
};
