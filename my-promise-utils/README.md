# Some utils for Promise
*  first,
*  last,
*  none,
*  map,
*  PromiseCollection,
*  createCollection,
*  cbWrapperNonEs6,
*  cbWrapper,

# Development
1. `yarn install`
2. `yarn dev` to start dev server at `http://localhost:3333`
3. `yarn test` to run unit test
