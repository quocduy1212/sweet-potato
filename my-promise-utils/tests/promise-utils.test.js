import {
  first,
  last,
  none,
  map,
  PromiseCollection,
  createCollection,
  cbWrapperNonEs6,
  cbWrapper,
  ajax,
} from '../app/js/promise-utils';

const createSuccessTimeoutPromise = timeout =>
  new Promise(resolve => setTimeout(() => resolve(timeout), timeout));

const createRejectedTimeoutPromise = timeout =>
  new Promise((resolve, reject) => setTimeout(() => reject(timeout), timeout));

test('first(promises) fullfilled promises', () => {
  first([
    createSuccessTimeoutPromise(100),
    createSuccessTimeoutPromise(200),
  ]).then(first => expect(first).toEqual(100));
  first([
    'smt',
    createSuccessTimeoutPromise(100),
    createSuccessTimeoutPromise(200),
  ]).then(first => expect(first).toEqual('smt'));
});

test('first(promises) rejected promises', () => {
  first([createRejectedTimeoutPromise(100), createSuccessTimeoutPromise(200)])
    .then(smt => expect(true).toEqual(false))
    .catch(err => expect(err).toEqual('Oops'));
});

test('wrapped ajax', () => {
  const wrapped = cbWrapper(ajax);
  wrapped('url here').then(success => expect(success).toEqual('success'));
});

test('wrapped ajax non es6', () => {
  const wrapped = cbWrapperNonEs6(ajax);
  wrapped('url here').then(success => expect(success).toEqual('success'));
});

test('last(promises) fullfilled promises', () => {
  last([
    createSuccessTimeoutPromise(100),
    createSuccessTimeoutPromise(200),
  ]).then(last => expect(last).toEqual(200));
  last([
    'smt',
    createSuccessTimeoutPromise(100),
    createSuccessTimeoutPromise(200),
  ]).then(last => expect(last).toEqual(200));
});

test('last(promises) rejected promises', () => {
  last([createRejectedTimeoutPromise(100), createSuccessTimeoutPromise(200)])
    .then(smt => expect(true).toEqual(false))
    .catch(err => expect(err).toEqual('Oops'));
});

test('none(promises) fullfilled promises', () => {
  none([createSuccessTimeoutPromise(100), createSuccessTimeoutPromise(200)])
    .then(none => expect(true).toEqual(false))
    .catch(err => expect(err).toEqual('Oops'));
});

test('none(promises) rejected promises', () => {
  none([createRejectedTimeoutPromise(100), createSuccessTimeoutPromise(200)])
    .then(msg => expect(err).toEqual('None'))
    .catch(() => expect(true).toEqual(false));
});

test('map(promises) fullfilled promises', () => {
  map(
    [createSuccessTimeoutPromise(100), createSuccessTimeoutPromise(200)],
    (value, resolve) => setTimeout(() => resolve(value * 2), 100)
  ).then(values => {
    expect(values.length).toEqual(2);
    expect(values[0]).toEqual(200);
    expect(values[1]).toEqual(400);
  });
});

test('PromiseCollection(promises) fullfilled promises', () => {
  PromiseCollection([100, 200])
    .map((value, resolve) => setTimeout(() => resolve(value * 2), 100))
    .map((value, resolve) => setTimeout(() => resolve(value * 2), 100))
    .val(values => {
      expect(values.length).toEqual(2);
      expect(values[0]).toEqual(100 * 2 * 2);
      expect(values[1]).toEqual(200 * 2 * 2);
    });
});

test('createCollection(promises) fullfilled promises', () => {
  createCollection([100, 200])
    .map((value, resolve) => setTimeout(() => resolve(value * 2), 100))
    .map((value, resolve) => setTimeout(() => resolve(value * 2), 100))
    .val(values => {
      expect(values.length).toEqual(2);
      expect(values[0]).toEqual(100 * 2 * 2);
      expect(values[1]).toEqual(200 * 2 * 2);
    });
});
