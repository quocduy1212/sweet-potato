import MyRegex from '../app/js/regex';

test('buildMatcher', () => {
  expect(new MyRegex('abc').buildMatcher(0).match('a')).toEqual(true);
  expect(new MyRegex('abc').buildMatcher(0).nextMatcherIndex).toEqual(1);

  expect(new MyRegex('abc').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('abc').buildMatcher(1).nextMatcherIndex).toEqual(2);

  expect(new MyRegex('a[b]c').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('a[b]c').buildMatcher(1).nextMatcherIndex).toEqual(4);

  expect(new MyRegex('a[cdb]c').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('a[cdb]c').buildMatcher(1).match('a')).toEqual(false);

  expect(new MyRegex('a[.]c').buildMatcher(1).match('b')).toEqual(true);

  expect(new MyRegex('a[.]c').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('a[.]c').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('a[.]c').buildMatcher(1).match('b')).toEqual(true);
  expect(new MyRegex('a[.]c').buildMatcher(1).match('b')).toEqual(true);
});
