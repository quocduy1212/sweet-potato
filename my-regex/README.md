# Quick implementation of regular expression, support:
1. `abc`
2. `abc.def`: match any chars
3. `abc.*def`: match any chars by any occurencies
4. `a[abc]def`: collection of char matcher
5. `a(bc.*d)ef`: collect results between ()
