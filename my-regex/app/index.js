import 'babel-polyfill';
import main from './js/main';
import './main.scss';

document.addEventListener('DOMContentLoaded', function(event) {
  console.log('DOM fully loaded and parsed');
  main();
});
