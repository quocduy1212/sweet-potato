import MyRegex from './regex';

export default () => {
  console.log('main');
  // console.log(new MyRegex('a.').match('ab'));
  // console.log(new MyRegex('a.bcd').match('axbcd'));
  // console.log(new MyRegex('a[.]*bcd').match('xxxxxxxwerbcd'));
  // console.log(new MyRegex('a([xyz]*)[def]*b').match('axzxyzydefb'));
  console.log(new MyRegex('a([x]*)a([abc]*)d').match('axxabbcabacd'));
  console.log(new MyRegex('a([x]*)([abc]*)d').match('axxabbcabacd'));
};
