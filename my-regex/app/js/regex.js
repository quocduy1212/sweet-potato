export default class MyRegex {
  constructor(pattern) {
    this.pattern = pattern;
  }

  /*
	“abc”
	“abc.def”
	“abc.*def”
	“a[abc]def”
	“a(bc.*d)ef”
	".*def" ==> "def" || ".*def"
	*/

  buildMatcher = patternIndex => {
    if (this.pattern[patternIndex] === '[') {
      const closeIndex = this.pattern.indexOf(']', patternIndex);
      const chars = this.pattern.substring(patternIndex + 1, closeIndex);
      return {
        match: char => chars.indexOf(char) >= 0 || chars.indexOf('.') >= 0,
        nextMatcherIndex: closeIndex + 1,
        pattern: chars,
      };
    } else {
      return {
        match: char =>
          char === this.pattern[patternIndex] ||
          this.pattern[patternIndex] === '.',
        nextMatcherIndex: patternIndex + 1,
        pattern: this.pattern[patternIndex],
      };
    }
  };

  isCollectorChars = char => '()'.indexOf(char) >= 0;

  matchUsingRecursive = (str, strIndex, patternIndex) => {
    if (patternIndex === this.pattern.length || strIndex === str.length) {
      return strIndex === str.length && strIndex === str.length;
    }

    let localPatternIndex = patternIndex;
    let skippedChar = '';
    while (this.isCollectorChars(this.pattern[localPatternIndex])) {
      skippedChar = `${this.pattern[localPatternIndex]}${skippedChar}`;
      localPatternIndex++;
    }

    const matcher = this.buildMatcher(localPatternIndex);
    const matched = matcher.match(str[strIndex]);
    const nextMatcherIndex = matcher.nextMatcherIndex;
    let result = false;

    if (
      nextMatcherIndex < this.pattern.length &&
      this.pattern[nextMatcherIndex] === '*'
    ) {
      result =
        this.matchUsingRecursive(str, strIndex, nextMatcherIndex + 1) ||
        (matched &&
          this.matchUsingRecursive(str, strIndex + 1, localPatternIndex));
    } else {
      result =
        matched &&
        this.matchUsingRecursive(str, strIndex + 1, nextMatcherIndex);
    }
    // console.log(
    // `${str.substring(strIndex)}, ${this.pattern.substring(
    // localPatternIndex
    // )} (${strIndex}, ${localPatternIndex}) ==> matched: ${result}  char: ${
    // str[strIndex]
    // } pattern: ${matcher.pattern} `
    // );

    if (matched) {
      this.currentMatch += str[strIndex] + skippedChar;
    }

    return result;
  };

  match = str => {
    this.currentMatch = '';
    const result = this.matchUsingRecursive(str, 0, 0, false);
    console.log(
      this.currentMatch
        .split('')
        .reverse()
        .join('')
    );
    return result;
  };
}
