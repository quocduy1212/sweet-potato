/* eslint-disable global-require */
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';

import 'app-styles/main';

import store from './store/configure-store';
import model from './store/configure-model';
import Root from './root';

render(
  <Root store={store} model={model} />,
  document.getElementById('phamduy-container')
);
