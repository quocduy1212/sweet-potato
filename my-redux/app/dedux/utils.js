export const isFunction = input => typeof input === 'function';
export const noop = () => {};
