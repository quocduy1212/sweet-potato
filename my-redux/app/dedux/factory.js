import Model from './model';
import Controller from './controller';
import { noop } from './utils';

let appController;
let modelWrapper;

const setupController = (initModel, actionHandlers) => {
  appController = new Controller(new Model(initModel), actionHandlers);
  modelWrapper = {
    getAppModel: () => appController.getAppModel(),
    send: action => appController.send(action),
  };
  return modelWrapper;
};

const createControlledView = (View, actions = {}, mapStateToProps = noop) => {
  return appController.setupView(View, actions, mapStateToProps);
};

export default createControlledView;
export { setupController };
