import React, { Component } from 'react';
import { isFunction } from './utils';

class Controller {
  model = {};
  constructor(model, handlers) {
    this.model = model;
    this.handlers = handlers;
  }

  getAppModel = () => this.model.getAppModel();

  send = action => {
    this.model.update(this.handlers, action);
  };

  handleAction = action => {
    if (isFunction(action)) {
      action(this.getAppModel, this.send);
    } else {
      this.model.update(this.handlers, action);
    }
  };

  setupActionHandler = actions => {
    const connectedActions = {};
    Object.keys(actions).forEach(k => {
      connectedActions[k] = (...args) => {
        this.handleAction(actions[k](...args));
      };
    });
    return connectedActions;
  };

  setupView = (View, actions, mapStateToProps) => {
    const self = this;
    const connectedActions = this.setupActionHandler(actions);
    return class extends Component {
      componentDidMount() {
        self.model.listen(this.onModelChange);
      }

      componentWillUnmount() {
        self.model.remove(this.onModelChange);
      }

      onModelChange = () => this.forceUpdate();

      render() {
        const appModel = self.model.getAppModel();
        return (
          <View {...mapStateToProps(appModel)} actions={connectedActions} />
        );
      }
    };
  };
}

export default Controller;
