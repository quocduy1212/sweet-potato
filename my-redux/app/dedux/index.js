import createControlledView, { setupController } from './factory';

export default createControlledView;
export { setupController };
