class Model {
  constructor(initModel = {}) {
    this.appModel = initModel;
    this.onChangeHandlers = [];
  }

  getAppModel = () => this.appModel;

  update = (handler, action) => {
    this.appModel = handler(this.appModel, action);
    // appModel change check here
    this.onChangeHandlers.forEach(h => h());
  };

  listen = onChangeHandler => {
    this.onChangeHandlers.push(onChangeHandler);
  };

  remove = onChangeHandler => {
    this.onChangeHandlers = this.onChangeHandlers.filter(
      h => h !== onChangeHandler
    );
  };
}

export default Model;
