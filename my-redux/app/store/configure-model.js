import { setupController } from '../dedux';
import handlers from '../handlers';

const model = setupController({ counter: 0 }, handlers);

export default model;
