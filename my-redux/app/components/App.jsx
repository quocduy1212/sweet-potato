import React from 'react';
import createControlledView from '../dedux';
import * as actions from '../actions';

import styles from './App.scss';

const App = ({ counter, actions }) => (
  <div className={styles.bold}>
    <div>{`counter: ${counter}`}</div>
    <button onClick={() => actions.increaseCounter()}>increase</button>
    <button onClick={() => actions.decreaseCounter()}>decrease</button>
  </div>
);

const mapStateToProps = model => ({
  ...model,
});

export default createControlledView(App, actions, mapStateToProps);
