const DEFAULT_STATE = {};

const handlers = {};

const app = (state = DEFAULT_STATE, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export default app;
