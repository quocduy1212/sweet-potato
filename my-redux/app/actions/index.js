export const increaseCounter = (by = 1) => (getAppModel, send) => {
  send({
    type: 'INC',
    by: getAppModel().counter || by,
  });
};

export const decreaseCounter = () => ({
  type: 'DEC',
});
