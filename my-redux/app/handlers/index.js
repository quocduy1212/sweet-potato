const handlers = (model, action) => {
  switch (action.type) {
    case 'INC':
      return {
        ...model,
        counter: model.counter + action.by,
      };
      break;
    case 'DEC':
      return {
        ...model,
        counter: model.counter - 1,
      };
      break;
    default:
      break;
  }
};

export default handlers;
