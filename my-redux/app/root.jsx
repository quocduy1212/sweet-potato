import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import App from './components/App';

const Root = ({ store, model }) => (
  <Provider store={store} model={model}>
    <App />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
  model: PropTypes.object.isRequired,
};

export default Root;
