# My quick implementation of Redux called dedux

## Functions
1. `createControlledView` act like redux `connect`
2. `setupController` act like redux `createStore`
3. Action can be object or `thunk`
4. `handlers` are like redux `reducers`

# Development
1. `yarn install`
2. `yarn dev` to start dev server at `http://localhost:3333`
3. `yarn test` to run unit test
