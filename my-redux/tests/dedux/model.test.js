import jest from 'jest-mock';
import Model from '../../app/dedux/model';

test('add/remove handler', () => {
  const model = new Model();
  const handler = () => {};
  model.listen(handler);
  expect(model.onChangeHandlers.length).toEqual(1);
  model.remove(handler);
  expect(model.onChangeHandlers.length).toEqual(0);
});

test('handler is call on change', () => {
  const model = new Model();
  const handler = jest.fn();
  model.listen(handler);
  model.update(input => input);
  expect(handler.mock.calls.length).toEqual(1);
});

test('model is updated when handler is call', () => {
  const model = new Model();
  const handler = (appModel, action) => ({
    ...appModel,
    newValue: 'updated',
  });
  model.update(handler, { type: 'type' });
  expect(model.appModel.newValue).toEqual('updated');
});
