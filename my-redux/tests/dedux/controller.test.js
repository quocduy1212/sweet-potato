import jest from 'jest-mock';
import Controller from '../../app/dedux/controller';

test('setupActionHandler', () => {
  const EXPEC_VALUE = 'smt';
  const model = {
    update: (handlers, action) => {
      handlers({}, action);
    },
  };
  const actions = {
    onChange: value => ({
      type: 'ON_CHANGE',
      value,
    }),
  };
  const handlers = (appModel, action) => {
    expect(action.type).toEqual('ON_CHANGE');
    expect(action.value).toEqual(EXPEC_VALUE);
  };
  const controller = new Controller(model, handlers);
  const result = controller.setupActionHandler(actions);
  result.onChange(EXPEC_VALUE);
});
